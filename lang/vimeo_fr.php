<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_vimeo' => 'Ajouter cette vimeo',
	'albums' => 'Faire des mots clefs les albums du compte',
	'archives' => 'Créer un mot clef pour archiver les vidéos par année',

	// C
	'cfg_titre_parametrages' => "Parametrons",
	'champ_credits_label' => 'Crédits',
	'champ_texte_label' => 'Texte',
	'champ_titre_label' => 'Titre',
	'champ_url_video_label' => 'Adresse de la vidéo',
	'channels' => 'Importer les noms de chaînes en tant que mots clefs?',
	'client_data' => '<a href="https://developer.vimeo.com/api/start">Créer vos id et secret sur l’api Vimeo</a>',
	'client_id' => 'Client ID vimeo',
	'client_secret' => 'Client Secret vimeo',
	'configuration_vimeo' => 'Configurer Vimeo',
	'confirmer_supprimer_vimeo' => 'Confirmez-vous la suppression de cette vimeo ?',

	// I
	'icone_creer_vimeo' => 'Créer une vimeo',
	'icone_modifier_vimeo' => 'Modifier cette vimeo',
	'id_auteur' => 'Quel auteur',
	'id_groupe_albums_explication' => 'A créer préalablement par vos soins',
	'id_groupe_albums' => 'A quel groupe de mots les joindre?',
	'id_groupe_archives_explication' => 'A créer préalablement par vos soins, basé sur la date de publication de votre vidéo chez Viméo',
	'id_groupe_archives' => 'A quel groupe de mots les joindre?',
	'id_groupe_channels_explication' => 'A créer préalablement par vos soins',
	'id_groupe_channels' => 'A quel groupe de mots les joindre?',
	'id_groupe_explication' => 'Identifiant du groupe des albums récoltés',
	'id_groupe' => 'Id du groupe de mots',
	'info_1_vimeo' => 'Une vimeo',
	'info_aucun_vimeo' => 'Aucune vimeo',
	'info_nb_vimeos' => '@nb@ vimeos',
	'info_vimeos_auteur' => 'Les vimeos de cet auteur',

	// L
	'label_credits' => 'Crédits',
	'label_texte' => 'Texte',
	'label_titre' => 'Titre',
	'label_url_vimeo' => 'Adresse de la vidéo',
	'liaison_auteur' => 'Lier les vidéos récupérées à l’auteur précité',

	// P
	'profil_auteur_statut' => 'Quel droit donner à cet auteur',
	'profil_auteur'=> 'Récupérer le profil et mettre à jour un auteur',
	'profil_new' => 'Récupérer le profil et en faire un nouvel auteur',
	'profil_rien' => 'Récupérer seulement les vidéos',
	'profil' => 'Profil vimeo',

	// R
	'recuperer_videos' => "Récuperer les videos",
	'retirer_lien_vimeo' => 'Retirer cette vimeo',
	'retirer_tous_liens_vimeos' => 'Retirer toutes les vimeos',

	// S
	'supprimer_vimeo' => 'Supprimer cette vimeo',

	// T
	'texte_ajouter_vimeo' => 'Ajouter une vimeo',
	'texte_changer_statut_vimeo' => 'Cette vimeo est :',
	'texte_creer_associer_vimeo' => 'Créer et associer une vimeo',
	'texte_definir_comme_traduction_vimeo' => 'Cette vimeo est une traduction de la vimeo numéro :',
	'titre_langue_vimeo' => 'Langue de cette vimeo',
	'titre_logo_vimeo' => 'Logo de cette vimeo',
	'titre_objets_lies_vimeo' => 'Liés à cette vimeo',
	'titre_page_configurer_vimeo' => 'Configurer Vimeo',
	'titre_vimeo' => 'Vimeo',
	'titre_vimeos_rubrique' => 'Vimeos de la rubrique',
	'titre_vimeos' => 'Vimeos',
	
	// U
	'url_vimeo' => 'Url du compte vimeo',
	'user_vimeo' => 'User Vimeo',
	'user_vimeo_explication' => 'Identifiant Vimeo de type 99487171',

	// V
	'verifier_checker' => 'Veuillez vérifier les informations ci dessous et valider.',
	'vimeo_statut' 	=> 'Statut des vidéos à leur récupération',

);
