<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'vimeo_description' => '',
	'vimeo_nom' => 'Vimeos',
	'vimeo_slogan' => 'Piocher les datas d\'une video vimeo et les cacher sur le site.',
);
