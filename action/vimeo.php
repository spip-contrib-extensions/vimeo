<?php

/**
 * Fonctions utiles au plugin Vimeo
 *
 * @plugin     Vimeo
 * @copyright  2014-2021
 * @author     Charles Stephan
 * @licence    GNU/GPL
 * @package    SPIP\Vimeo\Fonctions
 *
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/utils');
include_spip('inc/distant');
include_spip('inc/filtres');

function vimeo_creer_auteur($infos) {
	$infos = $infos["body"];

	$compte = lire_config('vimeo');

	$id = filter_var($infos["uri"], FILTER_SANITIZE_NUMBER_INT);

	$id_auteur = $compte['id_auteur'] ? $compte['id_auteur'] : $id;
	$bio = $infos["bio"];
	$bio = str_replace('[e]', '[en]', $bio);
	$bio = "<multi>" . $bio . "</multi>";
	$nom = $infos["name"];
	$logo_auteur = $infos["pictures"]["sizes"][3]["link"];

	$nom_site = $infos["websites"][0]["name"];
	$url_site = $infos["websites"][0]["link"];

	$statut = $compte["profil_auteur_statut"] ? $compte["profil_auteur_statut"] : "6forum";

	$k = recuperer_infos_distantes($logo_auteur);

	$portrait = copie_locale($logo_auteur, 'auto', 'IMG/auton' . $id_auteur . '.' . $k['extension']);

	$spip_auteurs = array(
		"id_auteur" => $id_auteur,
		"nom"       => $nom,
		"bio"       => $bio,
		"nom_site"  => $nom_site,
		"url_site"  => $url_site,
		"statut"    => $statut,
	);

	$where = "id_auteur = " . intval($id_auteur);
	$result = sql_countsel("spip_auteurs", $where);

	if ($result) {
		unset($spip_auteurs['id_auteur']);
		$spip_auteurs = sql_updateq('spip_auteurs', $spip_auteurs, $where);
	} else {
		$spip_auteurs = sql_insertq('spip_auteurs', $spip_auteurs);
	}

	return $id_auteur;
}

function vimeo_creer_albums($albums, $album_lier_vimeo) {
	$compte = lire_config('vimeo');
	vimeo_creer_mots($albums, $album_lier_vimeo, $compte['id_groupe_albums']);
}

function vimeo_creer_archives($archives, $archives_lier_vimeo) {
	$compte = lire_config('vimeo');
	vimeo_creer_mots($archives, $archives_lier_vimeo, $compte['id_groupe_archives']);
}

function vimeo_creer_channels($channels, $channel_lier_vimeo) {
	$compte = lire_config('vimeo');
	vimeo_creer_mots($channels, $channel_lier_vimeo, $compte['id_groupe_channels']);
}

function vimeo_creer_video($videos) {
	$spip_vimeos = array();
	$compte = lire_config('vimeo');
	// spip_log(print_r($compte, true), 'vimeo');
	spip_log(print_r($videos, true), 'vimeo_videos');

	$archives_lier_vimeo = array();
	$archives = array();

	foreach ($videos as $key => $value) {
		$id_vimeo = explode("/", $value["uri"]);
		$id_vimeo = $id_vimeo[2];

		$titre = $value["name"];

		$url_vimeo = "https://www.vimeo.com/" . $id_vimeo;

		$description = $value['description'];

		$description = str_replace('[e]', '##e##', $description);
		$description = str_replace('[en]', '##e##', $description);
		$description = str_replace('"', '”', $description);

		if ($compte["archives"] === "on") {
			$trads = extraire_trads($description);
			$texte = $trads['fr'];

			$texte = explode("***", $texte);
			$millesime = filter_var($texte[2], FILTER_SANITIZE_NUMBER_INT);
			$id_mot = vimeo_recuperer_mot($millesime, $compte['id_groupe_archives']);

			array_push($archives, $millesime);
			array_push($archives_lier_vimeo, array(
				'id_mot'   => $id_mot,
				'id_objet' => $id_vimeo,
				'objet'    => 'vimeo',
			));
		}

		$date = date('Y-m-d H:i:s', strtotime($value["created_time"]));
		$logo = end($value["pictures"]["sizes"]);
		$logo = $logo["link"];

		$description = str_replace("Année", "{{Année}}", $description);
		$description = str_replace("Production", "{{Production}}", $description);
		$description = str_replace("Year", "{{Year}}", $description);
		$description = str_replace("***", "", $description);

		$description = str_replace('##e##', '[en]', $description);

		$k = recuperer_infos_distantes($logo);
		$fichier = copie_locale($logo, 'modif', 'IMG/vimeoon' . $id_vimeo . '.' . $k['extension']);

		array_push(
			$spip_vimeos,
			array(
				"id_vimeo"  => $id_vimeo,
				"url_video" => $url_vimeo,
				"titre"     => $titre,
				"texte"     => "<multi>" . $description . "</multi>",
				"credits"   => "",
				"date"      => $date,
				"statut"    => $compte['vimeo_statut'],
			)
		);
	}

	$spip_vimeos = sql_replace_multi('spip_vimeos', $spip_vimeos);

	if (isset($compte["archives"]) and $compte["archives"] === "on") {
		vimeo_creer_archives($archives, $archives_lier_vimeo);
	}
}

function vimeo_creer_mots($objets, $mots_lier_vimeo, $id_groupe) {
	$type = sql_getfetsel('titre', 'spip_groupes_mots', 'id_groupe=' . intval($id_groupe));

	$mots = array();

	foreach ($objets as $value) {
		$nom = $value['name'];
		$id_mot = sql_getfetsel('id_mot', 'spip_mots', 'titre=' . sql_quote($nom) . ' AND id_groupe=' . $id_groupe);

		if (is_null($id_mot)) {
			$id_mot = sql_insertq('spip_mots', array(
				'titre' => $nom,
				'type' => $type,
				'id_groupe' => $id_groupe
			));
		}
		$mot = array(
			"id_mot"    => $id_mot,
			"titre"     => $nom,
			"id_groupe" => $id_groupe,
			"type"      => $type,
		);

		array_push($mots, $mot);
	}
	// spip_log(print_r($mots, true), 'vimeo');
	// spip_log(print_r($objets, true), 'vimeo');

	$spip_mot = sql_replace_multi('spip_mots', $mots);

	$mots_lier_vimeo = sql_replace_multi('spip_mots_liens', $mots_lier_vimeo);
}

function action_vimeo_dist($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction("securiser_action", "inc");
		$arg = $securiser_action();
	}

	$videos = array();
	$auteurs_lier_vimeo = array();
	$album_lier_vimeo = array();
	$albums_infos = array();
	$channels_infos = array();
	$channel_lier_vimeo = array();

	include_spip('lib/vimeo/autoload');

	$erreur = array();

	$compte = lire_config('vimeo');

	$lib = new \Vimeo\Vimeo($compte['client_id'], $compte['client_secret']);
	$user_id = $compte['user_vimeo'];

	$user = $lib->request('/users/' . $user_id);
	$id_auteur = "";
	// spip_log(print_r($user, true), 'vimeo');

	if ($compte['profil'] !== 'rien') {
		$id_auteur = vimeo_creer_auteur($user);
	}

	/**********
	 * Les albums
	 */
	$albums = $user["body"]["metadata"]["connections"]["albums"]["uri"];
	$albums = $lib->request($albums)["body"]["data"];
	// spip_log(print_r($albums, true), 'vimeo');

	foreach ($albums as $key => $value) {
		array_push($albums_infos, array('name' => $value["name"], 'uri' => $value["uri"]));
	}

	foreach ($albums_infos as $a_info) {
		$vid = $lib->request($a_info['uri'] . "/videos")["body"]["data"];
		// spip_log(print_r($vid, true), 'vimeo');

		$id_mot = vimeo_recuperer_mot($a_info['name'], $compte['id_groupe_albums']);

		foreach ($vid as $video) {
			array_push($videos, $video);

			$id_vimeo = explode("/", $video["uri"]);
			$id_vimeo = $id_vimeo[2];

			if (isset($compte['albums']) and $compte['albums'] == 'on') {
				array_push($album_lier_vimeo, array(
					'id_mot'   => $id_mot,
					'id_objet' => $id_vimeo,
					'objet'    => 'vimeo',
				));
			}

			array_push($auteurs_lier_vimeo, array(
				'id_auteur' => $id_auteur,
				'id_objet'  => $id_vimeo,
				'objet'     => 'vimeo',
			));
		}
	}
	/**********/

	/**********
	 * Les chaînes
	 */
	$channels = $user["body"]["metadata"]["connections"]["channels"]["uri"];
	$channels = $lib->request($channels)["body"]["data"];
	// spip_log(print_r($channels, true), 'vimeo');

	foreach ($channels as $key => $value) {
		array_push($channels_infos, array('name' => $value["name"], 'uri' => $value["uri"]));
		// spip_log(print_r($lib->request($value["uri"]."/videos"), true), 'vimeo');
	}

	foreach ($channels_infos as $a_info) {
		$vid = $lib->request($a_info['uri'] . "/videos")["body"]["data"];
		// spip_log(print_r($a_info, true), 'vimeo');

		$id_mot = vimeo_recuperer_mot($a_info['name'], $compte['id_groupe_channels']);

		foreach ($vid as $video) {
			array_push($videos, $video);

			$id_vimeo = explode("/", $video["uri"]);
			$id_vimeo = $id_vimeo[2];

			if (isset($compte['channels']) and $compte['channels'] == 'on') {
				array_push($channel_lier_vimeo, array(
					'id_mot'   => $id_mot,
					'id_objet' => $id_vimeo,
					'objet'    => 'vimeo',
				));
			}

			array_push($auteurs_lier_vimeo, array(
				'id_auteur' => $id_auteur,
				'id_objet'  => $id_vimeo,
				'objet'     => 'vimeo',
			));
		}
	}
	/**********/

	if (isset($compte["liaison_auteur"]) and $compte["liaison_auteur"]) {
		$auteurs_lier_vimeo = array_unique($auteurs_lier_vimeo, SORT_REGULAR);
		$auteurs_lier_vimeo = sql_replace_multi('spip_auteurs_liens', $auteurs_lier_vimeo);
	}

	if ($compte["albums"] === "on") {
		$album_lier_vimeo = array_unique($album_lier_vimeo, SORT_REGULAR);
		vimeo_creer_albums($albums_infos, $album_lier_vimeo);
	}

	if ($compte["channels"] === "on") {
		$channel_lier_vimeo = array_unique($channel_lier_vimeo, SORT_REGULAR);
		vimeo_creer_channels($channels_infos, $channel_lier_vimeo);
	}

	if (count($videos) === 0) {
		$videos = $user["body"]["metadata"]["connections"]["videos"]["uri"];
		$videos = $lib->request($videos)["body"]["data"];
		// spip_log(print_r($videos, true), 'vimeo');
	}

	vimeo_creer_video($videos);

	return 1;
}

/**
 * Récupérer l'id_mot associé à l'album ou la chaîne vimeo.
 * Si le mot n'existe pas, il sera créé.
 * 
 * @param  string $titre
 * @param  int|string $id_groupe
 * @return int
 */
function vimeo_recuperer_mot($titre, $id_groupe) {
	include_spip('base/abstract_sql');
	if (intval($id_groupe) == 0 or is_null($id_groupe)) {
		return false;
	}
	$titre = trim($titre);
	$type = sql_getfetsel('titre', 'spip_groupes_mots', 'id_groupe=' . intval($id_groupe));
	$id_mot = sql_getfetsel('id_mot', 'spip_mots', 'titre=' . sql_quote($titre) . ' AND id_groupe=' . $id_groupe);
	if (is_null($id_mot) or empty($id_mot)) {
		$id_mot = sql_insertq('spip_mots', array(
			'titre' => $titre,
			'type' => $type,
			'id_groupe' => $id_groupe
		));
	}

	return $id_mot;
}
