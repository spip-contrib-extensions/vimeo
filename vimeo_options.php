<?php
/**
 * Options au chargement du plugin Vimeo
 *
 * @plugin     Vimeo
 * @copyright  2017-2021
 * @author     Charles Stephan
 * @licence    GNU/GPL
 * @package    SPIP\Vimeo\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
