<?php
/**
 * Définit les autorisations du plugin Vimeo
 *
 * @plugin     Vimeo
 * @copyright  2017-2021
 * @author     Charles Stephan
 * @licence    GNU/GPL
 * @package    SPIP\Vimeo\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 *
 * @pipeline autoriser
 */
function vimeo_autoriser() {
}

/**
 * Autorisation de voir un élément de menu (vimeos)
 *
 * @param  string  $faire  Action demandée
 * @param  string  $type  Type d'objet sur lequel appliquer l'action
 * @param  int  $id  Identifiant de l'objet
 * @param  array  $qui  Description de l'auteur demandant l'autorisation
 * @param  array  $opt  Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_vimeos_menu_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de voir le bouton d'accès rapide de création (vimeo)
 *
 * @param  string  $faire  Action demandée
 * @param  string  $type  Type d'objet sur lequel appliquer l'action
 * @param  int  $id  Identifiant de l'objet
 * @param  array  $qui  Description de l'auteur demandant l'autorisation
 * @param  array  $opt  Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_vimeocreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('creer', 'vimeo', '', $qui, $opt);
}

/**
 * Autorisation de créer (vimeo)
 *
 * @param  string  $faire  Action demandée
 * @param  string  $type  Type d'objet sur lequel appliquer l'action
 * @param  int  $id  Identifiant de l'objet
 * @param  array  $qui  Description de l'auteur demandant l'autorisation
 * @param  array  $opt  Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_vimeo_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

/**
 * Autorisation de voir (vimeo)
 *
 * @param  string  $faire  Action demandée
 * @param  string  $type  Type d'objet sur lequel appliquer l'action
 * @param  int  $id  Identifiant de l'objet
 * @param  array  $qui  Description de l'auteur demandant l'autorisation
 * @param  array  $opt  Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_vimeo_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de modifier (vimeo)
 *
 * @param  string  $faire  Action demandée
 * @param  string  $type  Type d'objet sur lequel appliquer l'action
 * @param  int  $id  Identifiant de l'objet
 * @param  array  $qui  Description de l'auteur demandant l'autorisation
 * @param  array  $opt  Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_vimeo_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

/**
 * Autorisation de supprimer (vimeo)
 *
 * @param  string  $faire  Action demandée
 * @param  string  $type  Type d'objet sur lequel appliquer l'action
 * @param  int  $id  Identifiant de l'objet
 * @param  array  $qui  Description de l'auteur demandant l'autorisation
 * @param  array  $opt  Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_vimeo_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}
